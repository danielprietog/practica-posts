import React from 'react'
import { Heading, Button } from '@chakra-ui/core'
import { Link } from './../../routes'
import MainLayout from '../../src/layout/Main'

const Dashboard = () => {

  return (
    <MainLayout>
      <Heading as={'h1'}>Dashboard Page</Heading>

      <Link route={'index'}>
        <a>
          <Button>Back to Index</Button>
        </a>
      </Link>

    </MainLayout>
  )
}

export default Dashboard

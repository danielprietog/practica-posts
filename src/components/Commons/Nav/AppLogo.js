import React from 'react'
import { GlobalItem } from '@atlaskit/navigation-next'
import { DoTribe } from './DoTribe'

export const AppLogo = props => (
  <GlobalItem
    icon={DoTribe}
    id='logo'
    onClick={() => console.log('Logo clicked')}
  />
)

import React, { useState } from 'react'
import { Box } from '@chakra-ui/core'
import { AppSwitcherDrawerLoader } from './AppSwitcherDrawerLoader'
import {
  MenuGroup,
  Section,
  HeadingItem,
  LinkItem
} from '@atlaskit/menu';
import ShortcutIcon from '@atlaskit/icon/glyph/shortcut';
import { useFetchAppsAccess } from './hooks'

export const AppSwitcherDrawer = () => {
  const [accesses] = useState([])

  return <Box>
    <MenuGroup>
      <Section>
        <HeadingItem>Modules</HeadingItem>
        {
          accesses.length > 0 && accesses.map(({_id, name, url}) => (
            <div key={_id}>
              <LinkItem
                href={url}
                elemBefore={<ShortcutIcon />}
                description={'Go to this Module'}
              >
                {name}
              </LinkItem>
            </div>
          ))
        }
      </Section>
    </MenuGroup>
  </Box>

}

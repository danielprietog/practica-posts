import { Box } from '@chakra-ui/core'
import { HeaderSection } from '@atlaskit/navigation-next'
import ContainerHeader from '@atlaskit/navigation-next/dist/cjs/components/presentational/ContainerHeader'
import React from 'react'

/**
 *
 * @param header {text!: String, subText!: String, before?: Component}
 * @param options
 * @returns {*}
 * @constructor
 */
export const HandlerProductNavigation = ({header = {}, options = []}) => (
  <Box pt={header.text ? 5 : 0}>
    {
      header && (
        <HeaderSection>
          {({ css }) => (
            <Box p={3} borderBottom={'1px solid #E2E8F0'}>
              <ContainerHeader
                {...header}
              />
            </Box>
          )}
        </HeaderSection>
      )
    }

    {
      options && options.length > 0 && (options.map((option, index) => (<div key={index}>{option}</div>)))
    }
  </Box>
)

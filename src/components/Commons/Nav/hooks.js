import React, {useState} from 'react'
import { useQuery } from '@apollo/react-hooks'

import { ACCESS_SETUP, ACCESS_PROFILE, ACCESS_MODULES } from './gql'

export const useFetchSetupAccess = () => {
  const [setupAccess, setSetupAccess] = useState({})

  useQuery(ACCESS_SETUP, {
    ssr: false,
    onCompleted: data => {
      setSetupAccess(data.S_Access[0])
    }
  })

  return {
    setupAccess
  }
}

export const useFetchProfileAccess = () => {
  const [profileAccess, setProfileAccess] = useState({})

  useQuery(ACCESS_PROFILE, {
    ssr: false,
    onCompleted: data => {
      setProfileAccess(data.S_Access[0])
    }
  })

  return {
    profileAccess
  }
}

export const useFetchAppsAccess = () => {
  const [accesses, setAccesses] = useState([])

  const { loading} = useQuery(ACCESS_MODULES, {
    ssr: false,
    fetchPolicy: 'network-only',
    onCompleted: data => {
      setAccesses(data.S_Access)
    }
  })

  return {
    accesses,
    loadingAccess: loading
  }
}

import { colors } from '@atlaskit/theme'

export const NavUserMenuStyles = {
  color: colors.N800,
  border: `1px solid ${colors.N40}`,
  boxShadow:
    '0px 4px 8px rgba(9, 30, 66, 0.25), 0px 0px 1px rgba(9, 30, 66, 0.31)',
  borderRadius: 4,
  maxWidth: 180,
}

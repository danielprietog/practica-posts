import React from "react"
import ContentLoader from "react-content-loader"

export const AppSwitcherDrawerLoader = () => (
  <ContentLoader
    speed={2}
    width={400}
    height={160}
    viewBox="0 0 400 160"
    backgroundColor="#f3f3f3"
    foregroundColor="#ecebeb"
  >
    <rect x="0" y="0" rx="3" ry="3" width="410" height="30" />
    <rect x="0" y="39" rx="3" ry="3" width="410" height="30" />
    <rect x="0" y="80" rx="3" ry="3" width="410" height="30" />
    <rect x="0" y="120" rx="3" ry="3" width="410" height="30" />
  </ContentLoader>
)

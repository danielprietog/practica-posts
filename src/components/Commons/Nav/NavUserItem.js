import React, { useContext, useState } from 'react'
import { SessionContext } from '../../../contexts/sessionContext'
import { Avatar, Box, PseudoBox } from '@chakra-ui/core'
import { NavUserItemLoader } from './NavUserItemLoader'
import Popup from '@atlaskit/popup'
import { NavUserMenu } from './NavUserMenu'
import { useFetchSetupAccess, useFetchProfileAccess } from './hooks'

export const NavUserItem = props => {

  const [isOpen, setIsOpen] = useState(false)

  // const { setupAccess } = useFetchSetupAccess()
  // const { profileAccess } = useFetchProfileAccess()

  return <Box
    position='relative'
  >
    <PseudoBox
      d='grid'
      bg={'white'}
      transition='ease-in .2s'
      alignItems={'center'}
      textAlign='center'
      cursor='pointer'
      h={'100%'}
    >
      {
        !session.name
          ?
          (<Box w='32px'><NavUserItemLoader/></Box>)
          :
          (
            <Popup
              isOpen={isOpen}
              onClose={() => setIsOpen(false)}
              placement={'top'}
              content={() => (
                <NavUserMenu profileAccess={profileAccess} setupAccess={setupAccess} logout={logout} session={session}/>
              )}
              trigger={triggerProps => (
                <Box
                  {...triggerProps}
                  isSelected={isOpen}
                  onClick={() => setIsOpen(!isOpen)}
                >
                  <Avatar size='sm' name={`${session.name} ${session.lastName}`} src={session.userImg}/>
                </Box>
              )}
            />
          )
      }
      {/*<NavUserItemLoader />*/}
    </PseudoBox>
    <Box position={'absolute'} zIndex={9}>
      {/*<Collapse isOpen={isOpenMenu === 'avatarUser'} bg='white' rounded='md' boxShadow="md"
       borderWidth='1px'>
       <PseudoBox
       d='grid'
       bg={'white'}
       transition='ease-in .2s'
       alignItems={'center'}
       textAlign='center'
       h='30px'
       minW='100px'
       cursor='pointer'
       onClick={logout}>
       Logout
       </PseudoBox>
       </Collapse>*/}
    </Box>
  </Box>
}

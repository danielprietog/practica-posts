import React, { useState } from 'react'
import { Box } from '@chakra-ui/core'
import { GlobalItem } from '@atlaskit/navigation-next'
import Drawer from '@atlaskit/drawer';
import AppSwitcherIcon from '@atlaskit/icon/glyph/app-switcher'
import { AppSwitcherDrawer } from './AppSwitcherDrawer'

export const AppSwitcher = () => {

  const [isDrawerOpen, setIsDrawerOpen] = useState(false)

  const onOpen = () => {
    setIsDrawerOpen(true)
  }
  const onClose = () => {
    setIsDrawerOpen(false)
  }
  const onCloseComplete = () => {}
  const onOpenComplete = () => {}

  return (
    <Box>
      <Drawer
        onClose={onClose}
        onCloseComplete={onCloseComplete}
        onOpenComplete={onOpenComplete}
        isOpen={isDrawerOpen}
        width="wide"
      >
        <AppSwitcherDrawer />
      </Drawer>
      <GlobalItem
        icon={AppSwitcherIcon}
        id='test'
        onClick={onOpen}
      />
    </Box>)
}

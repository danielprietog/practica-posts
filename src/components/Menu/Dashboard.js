import React from 'react'
import { Link } from '../../../routes'
import Item from '@atlaskit/navigation-next/dist/cjs/components/presentational/Item'
import DashboardIcon from '@atlaskit/icon/glyph/dashboard'
import IssuesIcon from '@atlaskit/icon/glyph/issues'
import Group from '@atlaskit/navigation-next/dist/cjs/components/presentational/Group'
import { ConnectedProductNavigation, ConnectedViewRegistry } from '../Commons/Nav/HanlderInnerNavigation'
import PdfIcon from '@atlaskit/icon/glyph/pdf';

export const header = {
  text: 'App',
  subText: 'Example Menu',
  before: DashboardIcon
}

export const options = [
  <Group key={0} heading="Group heading" hasSeparator>
    <Link route='dashboard'>
      <Item before={DashboardIcon} text="Index"/>
    </Link>
    <Link route='sample1'>
      <Item before={IssuesIcon} text="Customer"/>
    </Link>
    <Item before={IssuesIcon} text="Item"/>
    <Item before={IssuesIcon} text="Item"/>
  </Group>,

  <Group>
    <ConnectedViewRegistry />
    <ConnectedProductNavigation
      primaryOption={{
        text: 'Documents',
        before: PdfIcon
      }}
      secondaryOptions={[
        <Group key={0} heading="Group heading" hasSeparator>
          <Link route='dashboard'>
            <Item before={DashboardIcon} text="Index"/>
          </Link>
          <Link route='sample1'>
            <Item before={IssuesIcon} text="Customer"/>
          </Link>
          <Item before={IssuesIcon} text="Item"/>
          <Item before={IssuesIcon} text="Item"/>
        </Group>,
        <Group key={1} heading="Group heading" hasSeparator>
          <Link route='dashboard'>
            <Item before={DashboardIcon} text="Index"/>
          </Link>
          <Link route='sample1'>
            <Item before={IssuesIcon} text="Customer"/>
          </Link>
          <Item before={IssuesIcon} text="Item"/>
          <Item before={IssuesIcon} text="Item"/>
        </Group>
      ]}
    />
  </Group>
]
